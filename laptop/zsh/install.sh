#!/bin/bash



done=".$(basename $(pwd)).done"
aliases="$HOME/.aliases"
zshenv="$HOME/.zshenv"
zshrc="$HOME/.zshrc"
syntax_hl_url="git://github.com/zsh-users/zsh-syntax-highlighting.git"
syntax_hl_repo="$HOME/workspace/repos/zsh"
syntax_hl_bin="$HOME/.bin"
syntax_hl_bin_link="$syntax_hl_bin/zsh-syntax-highlighting.zsh"


install() {
	if [ -f $done ]; then
		exit 0
	fi
	clean
	$(ln -s "$(pwd)/aliases" $aliases)
	$(ln -s "$(pwd)/zshenv" $zshenv)
	$(ln -s "$(pwd)/zshrc" $zshrc)
	$(touch "$zshenv.local")

	mkdir -p $syntax_hl_repo
	$(git clone $syntax_hl_url $syntax_hl_repo)
	mkdir -p $syntax_hl_bin
	$(ln -s "$syntax_hl_repo/zsh-syntax-highlighting.zsh" $syntax_hl_bin_link)
	chsh -s=$(which zsh)
	touch $done
}


clean() {
	if [ -h $aliases ]; then
		rm $aliases
	elif [ -f $aliases ]; then
		mv $aliases "$aliases.old"
	fi
	if [ -h $zshenv ]; then
		rm $zshenv
	elif [ -f $zshenv ]; then
		mv $zshenv "$zshenv.old"
	fi
	if [ -h $zshrc ]; then
		rm $zshrc
	elif [ -f $zshrc ]; then
		mv $zshrc "$zshrc.old"
	fi
	if [ -d $syntax_hl_repo ] && [ -f $done ]; then
		rm -rf $syntax_hl_repo
	fi
	if [ -h $syntax_hl_bin_link ]; then
		rm $syntax_hl_bin_link
	elif [ -f $syntax_hl_bin_link ]; then
		mv $syntax_hl_bin_link "$syntax_hl_bin_link.old"
	fi
	rm -f $done
}


if (($# != 1)); then
	echo "usage"
	exit 1
fi

if [[ $1 == "install" ]]; then
	install
elif [[ $1 == "clean" ]]; then
	clean
else
	echo "unkown command"
	exit 1
fi

exit 0
