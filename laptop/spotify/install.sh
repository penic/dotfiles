#!/bin/bash



done=".$(basename $(pwd)).done"
spotify_sources="/etc/apt/sources.list.d/spotify.list"
spotify="spotify-client"


install() {
	if [ -f $done ]; then
		exit 0
	fi
	sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys BBEBDCB318AD50EC6865090613B00F1FD2C19886 0DF731E45CE24F27EEEB1450EFDC8610341D9410
	echo "deb http://repository.spotify.com stable non-free" | sudo tee $spotify_sources
	sudo apt-get update
	sudo apt-get install $spotify
	touch $done
}


clean() {
	sudo apt-get purge $spotify
	sudo apt-get autoremove
	sudo rm -f $spotify_sources
	sudo apt-key del BBEBDCB318AD50EC6865090613B00F1FD2C19886 0DF731E45CE24F27EEEB1450EFDC8610341D9410
	rm -f $done
}


if (($# != 1)); then
	echo "usage"
	exit 1
fi

if [[ $1 == "install" ]]; then
	install
elif [[ $1 == "clean" ]]; then
	clean
else
	echo "unkown command"
	exit 1
fi

exit 0
