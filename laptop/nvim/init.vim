" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

" autocomplete
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }

" syntax check
Plug 'scrooloose/syntastic'

" python
Plug 'klen/python-mode'

" ctags
Plug 'majutsushi/tagbar'

" glsl
Plug 'tikhomirov/vim-glsl'

" dir tree
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }

" status bar
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" git
Plug 'airblade/vim-gitgutter'

" denite
Plug 'Shougo/denite.nvim', { 'do' : 'UpdateRemotePlugins' }

" indentLine
Plug 'Yggdroot/indentLine'

" mesonic
Plug 'igankevich/mesonic'

" scala
"Plug 'derekwyatt/vim-scala'

" Initialize plugin system
call plug#end()


" options
set updatetime=100
set incsearch
set encoding=utf8
set number
set relativenumber
set scrolloff=15
set mouse=a
set hidden
"let s:color_column_old = join(range(81,300), ',')
"windo let &colorcolumn=s:color_column_old
set hlsearch
set cursorline
" • « » ¬ ❯ ❮
set listchars=tab:»\ ,trail:•,nbsp:¬,extends:❯,precedes:❮
set list
set tabstop=4
set shiftwidth=4
set softtabstop=4
set noexpandtab
setlocal spelllang=en_us


" leader
let mapleader = ","

" word processing mode
au BufRead,BufNewFile *.txt,*.tex call WordProcModeInit()

function! WordProcModeInit()
	set fo+=tqjalw fo-=c
	let b:word_proc_mode = 1
	call deoplete#disable()
endfunction

function! WordProcModeToggle()
	if b:word_proc_mode
		set fo-=alw fo+=c
		let b:word_proc_mode = 0
		call deoplete#toggle()
	else
		set fo+=alw fo-=c
		let b:word_proc_mode = 1
		call deoplete#toggle()
	endif
endfunction

nnoremap <leader>wp :call WordProcModeToggle()<cr>


" vim-airline
let g:airline_theme='angr'
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1


" deoplete
" enable deoplete.
let g:deoplete#enable_at_startup = 1
nnoremap <leader>dp :call deoplete#toggle()<cr>


" syntastic
let g:syntastic_cpp_compiler_options='-std=c++17 -Wall -Wextra'
let g:syntastic_c_compiler_options='-std=c11 -Wall -Wextra'


" python mode
let g:pymode_python = 'python3'
let g:pymode_options_colorcolumn = 0
let g:pymode_lint_cwindow = 0
set foldlevel=99
" map space bar to open/close folds
nnoremap <space> za
vnoremap <space> zf


" ctags
let g:tagbar_ctags_bin = '/usr/bin/ctags'
"let g:tagbar_ctags_bin = '/usr/bin/ctags-exuberant'
"  some C vim options...
if has("eval")
	let c_gnu = 1
	let c_space_errors = 1
endif
" see https://github.com/majutsushi/tagbar/blob/master/doc/tagbar.txt
" default width: 40
let g:tagbar_width = 40
" <,tb> toggle tagbar
nnoremap <leader>tb :TagbarToggle<cr>:TagbarShowTag<cr>


" nerdtree
nnoremap <leader>nt :NERDTreeToggle<cr>


" Denite
nnoremap <leader>db :Denite buffer<cr>
nnoremap <leader>df :Denite file file_rec<cr>


" indentLine
let g:indentLine_color_term = 105
let g:indentLine_char = '•'


" bindings
tnoremap jk <C-\><C-n>
inoremap jk <ESC>

nnoremap <C-u> <C-f>
nnoremap <C-i> <C-b>

nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k

nnoremap <leader>l :bn<cr>
nnoremap <leader>h :bp<cr>

"noremap <C-H> <C-w>5>
"noremap <C-L> <C-w>5<
"noremap <C-J> <C-w>5+
"noremap <C-K> <C-w>k-

nnoremap <leader>bb :buffer 
nnoremap <leader>w :set invwrap<cr>

noremap <F2> :set invspell<cr>
noremap <F3> :setlocal spelllang=en_us<cr>
noremap <F4> :setlocal spelllang=de_20<cr>
noremap <F5> :setlocal spelllang=en_us,de_20<cr>


let python_highlight_all = 1
let c_gnu = 1


colorscheme 256-jungle-transparent
"colorscheme 256-jungle

" ugly hack
highlight! link Search IncSearch
