#!/bin/bash



done=".$(basename $(pwd)).done"
vim_config_path="$HOME/.config/nvim"
init_vim="$vim_config_path/init.vim"
colors="$vim_config_path/colors"
vim_plug_dir="$HOME/.local/share/nvim/site/autoload/plug.vim"
vim_plug_url="https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"


install() {
	if [ -f $done ]; then
		exit 0
	fi
	clean
	mkdir -p $vim_config_path
	ln -s "$(pwd)/init.vim" $init_vim
	ln -s "$(pwd)/colors" $colors

	curl -fLo $vim_plug_dir --create-dirs $vim_plug_url

	nvim +PlugInstall

	touch $done
}


clean() {
	if [ -h $init_vim ]; then
		rm $init_vim
	elif [ -f $init_vim ]; then
		mv $init_vim "$init_vim.old"
	fi
	if [ -h $colors ]; then
		rm $colors
	elif [ -d $colors ]; then
		echo "WARNING renaming $colors"
		mv $colors "$colors.old"
	fi
	rm -f $done
}


if (($# != 1)); then
	echo "usage"
	exit 1
fi

if [[ $1 == "install" ]]; then
	install
elif [[ $1 == "clean" ]]; then
	clean
else
	echo "unkown command"
	exit 1
fi

exit 0
