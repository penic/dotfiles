[Appearance]
AntiAliasFonts=true
BoldIntense=true
ColorScheme=np-dark
Font=Hack,9,-1,5,50,0,0,0,0,0
LineSpacing=0
UseFontLineChararacters=false

[General]
Name=np-default
Parent=FALLBACK/
ShowTerminalSizeHint=false

[Interaction Options]
MouseWheelZoomEnabled=false

[Scrolling]
ScrollBarPosition=2

[Terminal Features]
BlinkingCursorEnabled=true
FlowControlEnabled=true
