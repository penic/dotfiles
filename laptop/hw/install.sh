#!/bin/bash



done=".$(basename $(pwd)).done"
config_dir="$HOME/.config"
hw_dir="$config_dir/herbstluftwm"
conky_dir="$config_dir/conky"
compton_dir="$config_dir/compton"
gtk3_dir="$config_dir/gtk-3.0"
gtkrc2="$HOME/.gtkrc-2.0"
gtkrc3="$gtk3_dir/settings.ini"
matcha_reop_url="https://github.com/vinceliuice/matcha.git"
themedir="/usr/share/themes"
konsolerc="$config_dir/konsolerc"
konsole_dir="$HOME/.local/share"


install() {
	if [ -f $done ]; then
		exit 0
	fi
	clean
	$(ln -s "$(pwd)/hlwm" $hw_dir)
	$(ln -s "$(pwd)/conky" $conky_dir)
	$(ln -s "$(pwd)/compton" $compton_dir)
#	$(ln -s "$(pwd)/gtkrc-2.0" $gtkrc2)
#	mkdir -p $gtk3_dir
#	$(ln -s "$(pwd)/settings.ini" $gtkrc3)
#	$(ln -s "$(pwd)/konsolerc" $konsolerc)
#	$(ln -s "$(pwd)/konsole" $konsole_dir)
#	$(git clone $matcha_reop_url)
#	cd matcha
#	sudo ./Install
#	cd ../
#	rm -rf matcha
	touch $done
}


clean() {
	if [ -h $hw_dir ]; then
		rm $hw_dir
	elif [ -d $hw_dir ]; then
		echo "WARNING renaming $hw_dir"
		mv $hw_dir "$hw_dir.old"
	fi
	if [ -h $conky_dir ]; then
		rm $conky_dir
	elif [ -d $conky_dir ]; then
		echo "WARNING renaming $conky_dir"
		mv $conky_dir "$conky_dir.old"
	fi
	if [ -h $compton_dir ]; then
		rm $compton_dir
	elif [ -d $compton_dir ]; then
		echo "WARNING renaming $compton_dir"
		mv $compton_dir "$compton_dir.old"
	fi
	if [ -h $gtkrc2 ]; then
		rm $gtkrc2
	elif [ -f $gtkrc2 ]; then
		mv $gtkrc2 "$gtkrc2.old"
	fi
	if [ -h $gtkrc3 ]; then
		rm $gtkrc3
	elif [ -f $gtkrc3 ]; then
		mv $gtkrc3 "$gtkrc3.old"
	fi
#	if [ -h $konsolerc ]; then
#		rm $konsolerc
#	elif [ -f $konsolerc ]; then
#		mv $konsolerc "$konsolerc.old"
#	fi
#	if [ -h $konsole_dir ]; then
#		rm $konsole_dir
#	elif [ -f $konsole_dir ]; then
#		mv $konsole_dir "$konsole_dir.old"
#	fi
	rm -rf $themedir/Matcha*
	rm -f $done
}


if (($# != 1)); then
	echo "usage"
	exit 1
fi

if [[ $1 == "install" ]]; then
	install
elif [[ $1 == "clean" ]]; then
	clean
else
	echo "unkown command"
	exit 1
fi

exit 0
