#/bin/bash



ALARM="ALARM:"
MESSAGE=""
PTIME=0

if [ $# -eq 0 ]; then
	echo "Invalid arguments!"
	echo -e "Usage:\n\talarm <time> [<message>]"
	exit 1
fi

TIME=$(echo $1 | egrep "(^[0-9]+[dhms]?$)")

if [ -z $TIME ]; then
	echo "Invalid arguments!"
	echo -e "Usage:\n\talarm <time> [<message>]"
	exit 1
fi

COUNTER=0
for i in $@; do
	case $COUNTER in
		0)
			MESSAGE="$ALARM"
			COUNTER=$((COUNTER+1))
			;;
		1)
			if [[ "$2" == "-p" ]]; then
				PTIME=10
			else
				MESSAGE="$MESSAGE $i"
			fi
			COUNTER=$((COUNTER+1))
			;;
		*)
			MESSAGE="$MESSAGE $i"
			;;
	esac
done


echo "Set alarm for ${TIME}. Message: '${MESSAGE}'"
(
	sleep $TIME
	notify $PTIME "$MESSAGE"
) &
