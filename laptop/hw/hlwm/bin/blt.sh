#!/bin/bash

while true; do
	pacmd list-sinks
	echo -e -n "[     ]"
	sleep 1
	echo -e -n "\r[o    ]"
	sleep 1
	echo -e -n "\r[oo   ]"
	sleep 1
	echo -e -n "\r[ooo  ]"
	sleep 1
	echo -e -n "\r[oooo ]"
	sleep 1
	echo -e -n "\r[ooooo]"
	sleep 1
	echo ""
done
