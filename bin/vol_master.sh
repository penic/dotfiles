#!/bin/bash


if [ $# -ne 1 ]; then
	exit 1
fi

if [ $1 == "+" ]; then
	RL=$1
elif [ $1 == "-" ]; then
	RL=$1
else
	exit 1
fi


amixer -M set Master 2%$RL | egrep "Mono: Playback [0-9]+" | {
	IFS=$'[%' read -ra volume

	POWER_PER=${volume[1]}
	OUTPUT="$(echo $POWER_PER | dzen2-gdbar -bg grey -fg red -l "Master Vol. ") $POWER_PER%"
	echo $OUTPUT | dzen2 -p 1 -x 900 -y 600 -w 300 -h 100 -bg black
}
