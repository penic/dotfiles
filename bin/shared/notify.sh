#!/bin/bash



PTIME="1"
OUTPUT=""


case $# in
	1)
		OUTPUT=$1
		;;
	2)
		PTIME=$1
		OUTPUT=$2
		;;
	*)
		echo "Invalid arguments!"
		echo -e "Usage:\n\tnotify [<time>] <message>"
		exit 1
		;;
esac


FONT="-*-fixed-medium-*-*-*-22-*-*-*-*-*-*-*"


WI=$(dzen2-textwidth "$FONT" "$OUTPUT")
#	COUNT=${#OUTPUT}
#	echo $COUNT
#	echo $WI
if [ $WI -ge 350 ]; then
#	exit 1
	(
		COUNT=${#OUTPUT}
		echo $COUNT 1>&2
		for ((i=0; i<$PTIME; i++)); do
			echo ${OUTPUT:-1}
			sleep 1
		done
	) | dzen2 -p 1 -x 900 -y 600 -w 400 -h 75 -bg black -fn $FONT
else
	echo $OUTPUT | dzen2 -p $PTIME -x 900 -y 600 -w 400 -h 75 -bg black -fn $FONT
fi
