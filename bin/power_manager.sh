#!/bin/bash

BAT_PATH=/sys/class/power_supply/BAT0

POWER_NOW=$(cat $BAT_PATH/energy_now)
POWER_MAX=$(cat $BAT_PATH/energy_full)
POWER_PER=$((($POWER_NOW*100)/$POWER_MAX))
COLOR_FULL="white"
COLOR_EMPTY="#808080"

EXECUTE=""


gosuspend(){
	{
		i=30
		while (( "$i" >= "0" )); do
			OUTPUT="BATTERY CRITICAL! SUSPEND IN: $i $(echo $i | dzen2-gdbar -max 30 -min 0 -bg grey -fg red)"
			echo $OUTPUT
			i=$(($i-1))
			sleep 1
		done
		$($BINPATH/suspend.sh)
	} | dzen2 -x 900 -y 500 -w 400 -h 100 -bg black
}


battery() {
	POWER_NOW=$(cat $BAT_PATH/energy_now)
	POWER_PER=$((($POWER_NOW*100)/$POWER_MAX))
	if (( "$POWER_PER" <= "100" )); then
		COLOR_TIP=$COLOR_EMPTY
	else
		COLOR_TIP=$COLOR_FULL
	fi
	if (( "$POWER_PER" <= "15" )); then
		COLOR_CRITICAL="red"
	else
		COLOR_CRITICAL=$COLOR_FULL
	fi
	if [[ "$(cat $BAT_PATH/status)" == "Discharging" ]]; then
		echo -n "^ca(1,$EXECUTE)$(echo $POWER_PER | dzen2-gdbar -max 100 -min 0 -fg $COLOR_CRITICAL -bg $COLOR_EMPTY -w 14 -h 8 -nonl)^fg($COLOR_TIP)^r(2x4)^fg() ^fg($COLOR_CRITICAL)${POWER_PER}%^fg()^ca()"
	if (( "$POWER_PER" <= "7" )); then
		gosuspend &
	fi
	else
		echo -n "^ca(1,$EXECUTE)^i(/home/nicolas/.config/dotfiles/charging.xbm) ^fg($COLOR_CRITICAL)${POWER_PER}%^fg()^ca()"
	fi
}
