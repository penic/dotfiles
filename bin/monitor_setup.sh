#!/bin/bash



if [ $# -eq 0 ]; then
	exit 1
fi


VGA=$(xrandr -q | grep "VGA" | cut -d " " -f 1)
LVDS=$(xrandr -q | grep "LVDS" | cut -d " " -f 1)
HDMI=$(xrandr -q | grep "HDMI" | cut -d " " -f 1)


if [ $1 -eq 1 ]; then
	herbstclient lock
	xrandr --auto
	xrandr --output $HDMI --off
	xrandr --output $VGA --off
	herbstclient detect_monitors
	herbstclient reload
	herbstclient unlock
elif [ $1 -eq 2 ]; then
	herbstclient lock
	xrandr --auto
	xrandr --output $LVDS --off
	herbstclient detect_monitors
	herbstclient reload
	herbstclient unlock
elif [ $1 -eq 3 ]; then
	herbstclient lock
	xrandr --output $LVDS --mode 1366x768
	xrandr --output $VGA --mode 1024x768
	xrandr --output $HDMI --mode 1024x768
	herbstclient detect_monitors
	herbstclient reload
	herbstclient unlock
elif [ $1 -eq 4 ]; then
	herbstclient lock
	xrandr \
		--output $LVDS --off
	xrandr \
		--output $VGA --mode 1680x1050 \
		--output $VGA --pos 0x0
	xrandr \
		--output $HDMI --mode 1680x1050 \
		--output $HDMI --pos 1680x0
	herbstclient set_monitors 1680x1050+0+0 1680x1050+1680+0
	herbstclient reload
	herbstclient unlock
elif [ $1 -eq 5 ]; then
	herbstclient lock
	xrandr \
		--output $HDMI --mode 1920x1200 \
		--output $HDMI --pos 0x0 \
		--output $HDMI --primary
	xrandr \
		--output $LVDS --mode 1366x768 \
		--output $LVDS --pos 0x1200
	herbstclient set_monitors 1920x1200+0+0 1366x768+0+1200
	#herbstclient detect_monitors
	sleep 1
	herbstclient reload
	sleep 2
	herbstclient unlock
fi
