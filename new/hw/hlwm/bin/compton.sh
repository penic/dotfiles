#!/bin/bash


logfile=/home/nicolas/.compton.log

killall compton

echo -e "\n$(date)" >> $logfile
compton --config ~/.config/compton/compton.conf --show-all-xerrors >> $logfile &
#compton --config ~/.config/compton/compton.conf -b --show-all-xerrors >> $logfile
