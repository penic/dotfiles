#!/bin/bash


if [ $# -ne 1 ]; then
	exit 1
fi

if [ $1 == "+" ]; then
	RL=$1
elif [ $1 == "-" ]; then
	RL=$1
else
	exit 1
fi



xbacklight -time 0 -steps 1 ${RL}10

xbacklight -get | awk '{print int($1+0.5)}' | {
	IFS=$'.' read -ra brightness

	POWER_PER=${brightness[0]}
	OUTPUT="$(echo $POWER_PER | dzen2-gdbar -bg grey -fg red -l "Brightness ") $POWER_PER%"
	echo $OUTPUT | dzen2 -p 1 -x 900 -y 600 -w 300 -h 100 -bg black
}
