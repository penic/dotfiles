#!/usr/bin/env python3.5

import sys
import subprocess



def main():
    hc = subprocess.Popen(["herbstclient", "-i"], stdout=subprocess.PIPE)
    print("started subprocess")
    for l in hc.stdout:
        print("It's a line!")
        print(l.decode('utf-8'))
    sys.exit(0)


if __name__ == '__main__':
    main()
