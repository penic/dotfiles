#!/bin/bash


OUTDIR=$HOME/Pictures/screenshots
OUTFILE=$(echo "screenshot_$(date +%yy%mm%dd_%Hh%Mm%Ss).jpg")

mkdir -p $OUTDIR
import -define jpg -window root $OUTDIR/$OUTFILE
