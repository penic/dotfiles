#!/bash/bin


TOUCH_PAD_ID=$(xinput | grep "TouchPad" | cut -d= -f 2 | cut -f 1)


touchpad_status() {
#	xinput --watch-props $TOUCH_PAD_ID | {
#	while true; do
#		read -r line
#		echo $line | grep "Device Enabled "
#		echo ""
#	done &
#}
	#xinput --watch-props $TOUCH_PAD_ID &
	xinput --disable $TOUCH_PAD_ID
}


touchpad() {
	STATUS=$(xinput --list-props $TOUCH_PAD_ID | grep "Device Enabled" | cut -d: -f 2 | cut -f 2)
	if [[ $STATUS == 0 ]]; then
		echo -n "^ca(1,xinput --enable $TOUCH_PAD_ID)TouchPad off^ca()"
	elif [[ $STATUS == 1 ]]; then
		echo -n "^ca(1,xinput --disable $TOUCH_PAD_ID)TouchPad  on^ca()"
	fi
}
