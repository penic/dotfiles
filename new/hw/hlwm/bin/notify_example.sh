#!/bin/bash

POWER_NOW=$(cat /sys/class/power_supply/BAT0/energy_now)
POWER_MAX=$(cat /sys/class/power_supply/BAT0/energy_full)

POWER_PER=$((($POWER_NOW*100)/$POWER_MAX))

OUTPUT="$(echo "40" | dzen2-gdbar -bg grey -fg white -bg \#002000 -l "Battery ")  ${POWER_PER}%"

echo $OUTPUT | dzen2 -p 5 -x 900 -y 600 -w 300 -h 100 -bg black
