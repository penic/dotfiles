#!/bin/bash

BINPATH=~/.config/dotfiles/bin
POWER_STATUS="/sys/class/power_supply/BAT0/status"

POWER_NOW=$(cat /sys/class/power_supply/BAT0/energy_now)
POWER_MAX=$(cat /sys/class/power_supply/BAT0/energy_full)
POWER_PER=$((($POWER_NOW*100)/$POWER_MAX))


notify(){
	OUTPUT="$(echo $POWER_PER | dzen2-gdbar -bg grey -fg red -l "Battery ")  ${POWER_PER}%"
	echo $OUTPUT | dzen2 -p 5 -x 900 -y 600 -w 400 -h 100 -bg black
}

gosuspend(){
	{
		i=30
		while (( "$i" >= "0" )); do
			OUTPUT="BATTERY CRITICAL! SUSPEND IN: $i $(echo $i | dzen2-gdbar -max 30 -min 0 -bg grey -fg red)"
			echo $OUTPUT
			i=$(($i-1))
			sleep 1
		done
		$($BINPATH/suspend.sh)
	} | dzen2 -x 900 -y 500 -w 400 -h 100 -bg black
}

check(){
	POWER_NOW=$(cat /sys/class/power_supply/BAT0/energy_now)
	POWER_PER=$((($POWER_NOW*100)/$POWER_MAX))
	echo $POWER_PER
	if (( "$POWER_PER" <= "15" )); then
		notify &
		if (( "$POWER_PER" <= "8" )); then
			gosuspend &
		fi
	fi
}

while true; do
	if [[ "$(cat $POWER_STATUS)" == "Discharging" ]]; then
		echo "Discharging"
		check
	else
		echo "Charging"
	fi
	sleep 60
done
