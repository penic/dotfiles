#!/bin/bash


TMPDIR=~/.tmp

mkdir -p $TMPDIR
import -window root $TMPDIR/tmp0.png
#convert $TMPDIR/tmp0.png -blur 0x4 $TMPDIR/tmp1.png
#convert $TMPDIR/tmp0.png -adaptive-blur 0x4 -normalize $TMPDIR/tmp1.png
convert $TMPDIR/tmp0.png -blur 0x4 -normalize $TMPDIR/tmp1.png
i3lock -i $TMPDIR/tmp1.png -t -d -I 10 -e -f
rm $TMPDIR/tmp0.png $TMPDIR/tmp1.png
