#!/bin/bash

trayer --tint "#101010" \
	--align right \
	--edge top \
	--widthtype request \
	--height 16 \
	--transparent true \
	--alpha 255 \
	--monitor primary
