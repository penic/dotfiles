#!/bin/bash


STATUS=5

if awk -Wv 2>/dev/null | head -1 | grep -q '^mawk'; then
	# mawk needs "-W interactive" to line-buffer stdout correctly
	# http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=593504
	uniq_linebuffered() {
		awk -W interactive '$0 != l { print ; l=$0 ; fflush(); }' "$@"
	}
else
	# other awk versions (e.g. gawk) issue a warning with "-W interactive", so
	# we don't want to use it there.
	uniq_linebuffered() {
		awk '$0 != l { print ; l=$0 ; fflush(); }' "$@"
	}
fi

{
	xinput --watch-props 12 > >(uniq_linebuffered)
} | {
	while true; do
		read -r TEST11
		echo $TEST11
		sleep 1
		echo "test"
	done
}

#{
#xinput --watch-props 12 | {
#	while true; do
#		echo "test"
#		read line
#		#echo $line | grep "Device Enabled "
#		echo "penis"
#	done
#}
#} &

while true; do
	sleep 1
	if [[ $STATUS = 0 ]]; then
		echo " off"
	elif [[ $STATUS = 1 ]]; then
		echo "  on"
	elif [[ $STATUS = 2 ]]; then
		exit 0
	fi
done
