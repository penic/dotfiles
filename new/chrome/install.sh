#!/bin/bash



done=".$(basename $(pwd)).done"
chrome_sources="/etc/apt/sources.list.d/google-chrome.list"
chrome="google-chrome-stable"


install() {
	if [ -f $done ]; then
		exit 0
	fi
	wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
	echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" | sudo tee $chrome_sources
	sudo apt-get update
	sudo apt-get install $chrome
	touch $done
}


clean() {
	sudo apt-get purge $chrome
	sudo apt-get autoremove
	sudo rm -f $chrome_sources
	sudo apt-key del 4CCA1EAF950CEE4AB83976DCA040830F7FAC5991 EB4C1BFD4F042F6DDDCCEC917721F63BD38B4796
	rm -f $done
}


if (($# != 1)); then
	echo "usage"
	exit 1
fi

if [[ $1 == "install" ]]; then
	install
elif [[ $1 == "clean" ]]; then
	clean
else
	echo "unkown command"
	exit 1
fi

exit 0
