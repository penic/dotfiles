#!/bin/bash



done=".$(basename $(pwd)).done"
target="$HOME/.gitconfig"


install() {
	if [ -f $done ]; then
		exit 0
	fi
	clean
	$(ln -s "$(pwd)/gitconfig" $target)
	touch $done
}


clean() {
	if [ -h $target ]; then
		rm $target
	elif [ -f $target ]; then
		mv $target "$target.old"
	fi
	rm -f $done
}


if (($# != 1)); then
	echo "usage"
	exit 1
fi

if [[ $1 == "install" ]]; then
	install
elif [[ $1 == "clean" ]]; then
	clean
else
	echo "unkown command"
	exit 1
fi

exit 0
