#!/bin/bash



done=".$(basename $(pwd)).done"


install() {
	if [ -f $done ]; then
		exit 0
	fi
	touch $done
}


clean() {
	rm -f $done
}


if (($# != 1)); then
	echo "usage"
	exit 1
fi

if [[ $1 == "install" ]]; then
	install
elif [[ $1 == "clean" ]]; then
	clean
else
	echo "unkown command"
	exit 1
fi

exit 0
