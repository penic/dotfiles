#!/bin/bash


install() {
	packages=
	for i in $(ls); do
		if [ -d $i ]; then
			packages="$packages $(cat $i/packages)"
		fi
	done
	echo $packages
	sudo apt update
	sudo apt install $packages

#	exit 0

	for i in $(ls); do
		if [ -d $i ]; then
			echo $i
			cd $i
			./install.sh install
			cd ..
		fi
	done
}


clean() {
	echo "clean"
}


if (($# != 1)); then
	echo "usage"
	exit 1
fi

if [[ $1 == "install" ]]; then
	install
elif [[ $1 == "clean" ]]; then
	clean
else
	echo "unkown command"
	exit 1
fi

exit 0
