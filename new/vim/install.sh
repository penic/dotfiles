#!/bin/bash



done=".$(basename $(pwd)).done"
vim_path="$HOME/.vim"
vim_path_bundle="$vim_path/bundle"
vim_path_bundle_link="$vim_path_bundle/Vundle.vim"
vimrc="$HOME/.vimrc"
colors="$vim_path/colors"
spell="$vim_path/spell"
bundle_repo="$HOME/workspace/repos/vim/"
bundle_url="https://github.com/VundleVim/Vundle.vim.git"


install() {
	if [ -f $done ]; then
		exit 0
	fi
	clean
	mkdir -p $vim_path
	$(ln -s "$(pwd)/vimrc" $vimrc)
	$(ln -s "$(pwd)/colors" $colors)
	$(ln -s "$(pwd)/spell" $spell)

	mkdir -p $bundle_repo
	$(git clone $bundle_url $bundle_repo)
	mkdir -p $vim_path_bundle
	$(ln -s $bundle_repo $vim_path_bundle_link)
	#$(vi +PluginInstall +qa)
	touch $done
}


clean() {
	if [ -h $vimrc ]; then
		rm $vimrc
	elif [ -f $vimrc ]; then
		mv $vimrc "$vimrc.old"
	fi
	if [ -h $colors ]; then
		rm $colors
	elif [ -d $colors ]; then
		echo "WARNING renaming $colors"
		mv $colors "$colors.old"
	fi
	if [ -h $spell ]; then
		rm $spell
	elif [ -d $spell ]; then
		echo "WARNING renaming $spell"
		mv $spell "$spell.old"
	fi
	if [ -h $vim_path_bundle ]; then
		rm $vim_path_bundle
	elif [ -d $vim_path_bundle ]; then
		echo "WARNING renaming $vim_path_bundle"
		mv $vim_path_bundle "$vim_path_bundle.old"
	fi
	if [ -d $bundle_repo ] && [ -f $done ]; then
		rm -rf $bundle_repo
	fi
	rm -f $done
}


if (($# != 1)); then
	echo "usage"
	exit 1
fi

if [[ $1 == "install" ]]; then
	install
elif [[ $1 == "clean" ]]; then
	clean
else
	echo "unkown command"
	exit 1
fi

exit 0
