#!/bin/bash



done=".$(basename $(pwd)).done"
gdb_init="$HOME/.gdbinit"


install() {
	if [ -f $done ]; then
		exit 0
	fi
	clean

	ln -s "$(pwd)/gdbinit" $gdb_init

	touch $done
}


clean() {
	if [ -h $gdb_init ]; then
		rm $gdb_init
	elif [ -f $gdb_init ]; then
		mv $gdb_init "$gdb_init.old"
	fi
	rm -f $done
}


if (($# != 1)); then
	echo "usage"
	exit 1
fi

if [[ $1 == "install" ]]; then
	install
elif [[ $1 == "clean" ]]; then
	clean
else
	echo "unkown command"
	exit 1
fi

exit 0
