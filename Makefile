.PHONY: all clean distclean install link_bin_files uninstall



DOTFILE_DIR = $(shell pwd)

BIN_DIR = $(DOTFILE_DIR)/bin
SHARED_DIR = $(BIN_DIR)/shared
CONFIG_DIR = $(DOTFILE_DIR)/config

DOTCON_DIR = ~/.config
HLWM_DIR = ~/.config/herbstluftwm
CONKY_DIR = ~/.config/conky
HWBIN_DIR = ~/.bin/hw_bin

all: install

install: herbstluftwm conky compton link_bin_files zshrc vimrc

herbstluftwm: #install_herbstluftwm
	mkdir -p $(HLWM_DIR)
	ln -s $(CONFIG_DIR)/autostart $(HLWM_DIR)/
	ln -s $(BIN_DIR)/panel.sh $(HLWM_DIR)/

conky: #install_conky
	mkdir -p $(CONKY_DIR)
	ln -s $(CONFIG_DIR)/conkyrc $(CONKY_DIR)/conky.conf

compton: #install_compton
	ln -s $(CONFIG_DIR)/compton.conf $(DOTCON_DIR)/

link_bin_files:
	mkdir -p $(HWBIN_DIR)
	ln -s $(SHARED_DIR)/* $(HWBIN_DIR)/

vimrc:
	ln -s $(CONFIG_DIR)/vimrc ~/.vimrc

zshrc:
	ln -s $(CONFIG_DIR)/aliases ~/.aliases
	ln -s $(CONFIG_DIR)/zshrc ~/.zshrc
	ln -s $(CONFIG_DIR)/zshenv ~/.zshenv

clean:
	rm -f $(HLWM_DIR)/autostart
	rm -f $(HLWM_DIR)/panel.sh
	rm -f $(CONKY_DIR)/conky.conf
	rm -f $(DOTCON_DIR)/compton.conf
	rm -f $(HWBIN_DIR)/*

distclean: uninstall

uninstall:
	echo $(DOTFILE_DIR)
